import imp
import matplotlib.pyplot as plt
from utils import normalizeElem, denormalizeElem

def     display_plot(thetra0, thetra1, mileages, prices):
    lineX = [float(min(mileages)), float(max(mileages))]
    lineY = []

    for elem in lineX:
        elem = thetra1 * normalizeElem(mileages, elem) + thetra0
        lineY.append(denormalizeElem(prices, elem))

    plt.figure(1)
    plt.plot(mileages, prices, 'bo', lineX, lineY, 'r-')
    plt.xlabel('mileage')
    plt.ylabel('price')
    plt.show()