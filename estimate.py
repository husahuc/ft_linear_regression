from fileinput import filename
from utils import normalizeElem, denormalizeElem, getPath, get_dataset
import sys
import csv

def get_estimate_price(theta0, theta1, mileage):
    mileages, prices = get_dataset()
    print(theta0, theta1, normalizeElem(mileages, mileage))
    price = theta0 + normalizeElem(mileages, mileage) * theta1
    return denormalizeElem(prices, price)

def get_mileage():
    while (1):
        print("Enter a mileage:")
        try:
            mileage = input()
        except:
            sys.exit("input error")
        try:
            mileage = int(mileage)
            if (mileage >= 0):
                break
            print("mileage must be superior to 0")
        except ValueError:
            print("not a number")
    return mileage

def get_thetas():
    theta0, theta1 = 0, 0
    filename = getPath("thetas.csv")
    try:
        with open(filename, 'r') as csvfile:
            csv_reader = csv.reader(csvfile, delimiter=',')
            for row in csv_reader:
                theta0 = float(row[0])
                theta1 = float(row[1])
                break
    except IOError:
        print("No thetas file")

    return theta0, theta1

def main():
    theta0, theta1 = get_thetas()
    mileage = get_mileage()
    price = get_estimate_price(theta0, theta1, mileage)
    print(f"the price for {mileage} is : {price}")

if __name__ == "__main__":
    main()