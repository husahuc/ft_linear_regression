import os
import csv

def	getPath(file):
	return os.path.join(os.path.dirname(__file__), file)

def	normalizeElem(list, elem):
	return ((elem - min(list)) / (max(list) - min(list)))

def	denormalizeElem(list, elem):
	return ((elem * (max(list) - min(list))) + min(list))

def get_dataset():
    mileages, prices = [], []
    filename = getPath("data.csv")
    try:
        with open(filename, 'r') as csvfile:
            csvReader = csv.reader(csvfile, delimiter=',')
            for row in csvReader:
                mileages.append(row[0])
                prices.append(row[1])
        mileages.pop(0)
        prices.pop(0)

        for i in range(len(mileages)):
            mileages[i] = int(mileages[i])
            prices[i] = int(prices[i])
    except IOError:
        pass
    return mileages, prices