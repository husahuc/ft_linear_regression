import csv
from fileinput import filename
import sys

from utils import getPath, get_dataset
from display import display_plot

def normalise_dataset(milleages, prices):
    x = []
    y = []
    for milleage in milleages:
        x.append((milleage - min(milleages)) / (max(milleages) - min(milleages)))
    for price in prices:
        y.append((price - min(prices)) / (max(prices) - min(prices)) )
    return (x, y)

def store_data(theta0, theta1):
    filename = getPath("thetas.csv")
    with open(filename, 'w') as csvfile:
        csv_writer = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        csv_writer.writerow([theta0, theta1])

def derivative(theta0, theta1, milleages, prices):
    tmp_theta0 = 0.0
    tmp_theta1 = 0.0
    for i in range(len(milleages)):
        tmp_theta0 += ((theta0 + (theta1 * milleages[i])) - prices[i])
        tmp_theta1 += ((theta0 + (theta1 * milleages[i])) - prices[i]) * milleages[i]
    derivative0 = (1 / len(milleages)) * tmp_theta0
    derivative1 = (1 / len(milleages)) * tmp_theta1
    return derivative0, derivative1

def linear_regression(mileages, prices):
    theta0 = 0.0
    theta1 = 0.0
    max_iterations = 1000
    learning_rate = 0.5

    for iteration in range(max_iterations):
        derivative0, derivative1 = derivative(theta0, theta1, mileages, prices)

        theta0 -= learning_rate * derivative0
        theta1 -= learning_rate * derivative1
        print(theta0, theta1)

    return theta0, theta1

def main():
    mileages, prices = get_dataset()
    x, y = normalise_dataset(mileages, prices)
    theta0, theta1 = linear_regression(x, y)
    store_data(theta0, theta1)
    if (len(sys.argv) > 1 and sys.argv[1] and sys.argv[1] == "--display"):
        display_plot(theta0, theta1, mileages, prices)

if __name__ == "__main__":
    main()